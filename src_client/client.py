
"""
client app
"""
import io
import zipfile
import argparse
import subprocess
import base64
import requests
import os

API_URL = "https://eu-gb.functions.cloud.ibm.com/api/v1/namespaces/sasori.axele%40gmail.com_dev/actions/resize?blocking=true"

class Client():
    """
    Object for testing endpoints, it parse arguments of script and send requests
    """
    def __init__(self):
        parser = argparse.ArgumentParser(
            description="Client to send requests to resize image")
        parser.add_argument("-i", "--input", action='store',
                            type=str,
                            help="Path to image to send",
                            required=False,
                            default='data/example.png')
        parser.add_argument("-o", "--output", action='store',
                            type=str,
                            help="Path to response" +
                            "zip/extracted images directory",
                            required=False, default='data/resized_images.zip')
        parser.add_argument("-e", "--extract",
                            help="Flag to extract response zip",
                            action='store_true', required=False)
        self.args = parser.parse_args()
        self.url = API_URL
        self.auth = (os.environ['IBM_API_USER'], os.environ['IBM_API_PASSWORD'])

    def post(self):
        """
        Send image as post request
        """
        with open(self.args.input, 'rb') as image_file:
            encoded_string = base64.b64encode(image_file.read())

        response = requests.post(self.url, json={'image': encoded_string.decode('utf-8')},
                                 auth=self.auth)

        if self.args.extract:
            if self.args.output == 'data/resized_images.zip':
                self.args.output = 'data/resized_images'
            in_memory_zipfile = io.BytesIO(base64.b64decode(response.text))
            zipfile.ZipFile(in_memory_zipfile).extractall(self.args.output)
        else:
            with open(self.args.output, 'wb') as output_file:
                output_file.write(base64.b64decode(response.text))

Client().post()

# IBM Cloud Functions

## Deploy

Для разработки требуется установить **IBM Cloud CLI**.

Для mac (Подробнее см. https://cloud.ibm.com/docs/cli/reference/ibmcloud?topic=cloud-cli-install-ibmcloud-cli):
```
$ curl -fsSL https://clis.cloud.ibm.com/install/osx | sh
```

Для простоты разработка будет вестись с аккаунта администратора, для того чтобы добавить нового пользователя может использоваться интерфейс **IBM IAM**.

Авторизация **IBM Cloud CLI** (Потребуется ввести пароль и регион IBM Cloud - в нашем случае eu-gb):
```
$ ibmcloud login -a cloud.ibm.com -o "sasori.axele@gmail.com" -s "dev"
```

Потребуется установить плагин **Cloud Function**:
```
$ ibmcloud plugin install cloud-functions
```

Назначить **resource-group**:
```
$ ibmcloud target -g Default
```

Назначить **namespace**:
```
$ ibmcloud target -o sasori.axele@gmail.com -s dev
```

Проверить настройку можно следующей командой:
```
$ ibmcloud fn list
```

Создать функцию можно с помощью команды:
```
$ ibmcloud fn action create resize --main resize --kind python:3.7 src/resize/main.py
```

Обновить код функции можно следующим способом:
```
$ ibmcloud fn action update resize --main resize --kind python:3.7 src/resize/main.py
```

Также присутствует поддержка загрузки виртуальных окружений python в zip-архиве, но runtime уже содержит некоторый набор сторонних библиотек и в данной ситуации это не было необходимо.

## Test

Для тестирования можно использовать скрипт **src_client/client.py**.

Для работы скрипта необходимы переменные среды IBM_API_USER и IBM_API_PASSWORD. Их значения можно получить у администратора проекта.

Описание флагов скрипта:
- -h, --help

    Отображает меню с помощью.
- -e, --extract

    Флаг указывает, требуется ли разархивировать полученный от сервера архив с изображениями.

- -i INPUT, --input INPUT

    INPUT указывает путь к файлу, который требуется отправить. По умолчанию **INPUT=data/example.png**
- -o OUTPUT, --output OUTPUT

    OUTPUT указывает путь для сохранения изображений / архива из ответа сервера. По умолчанию, если указан флаг -e **OUTPUT=data/resized_images**, иначе **OUTPUT=data/resized_images.zip**

